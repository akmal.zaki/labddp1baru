#Nama  = Akmal Zaki
#Kelas = DDP-1
#Npm   = 2106752205

from tkinter import *
from tkinter.filedialog import asksaveasfilename, askopenfilename


class Application(Frame):
    # melakukan inisialisasi frame. Kita membuat nilai default dari master pada Frame ini adalah None
    def __init__(self, master=None):
        # super berfungsi untuk mendapatkan parent dari child ini. Kita melakukan inisialisasi pada Frame
        # dengan memberi nilai master dan juga mengubah warna background
        super().__init__(master, bg="#252526")
        # kita mengatur nilai master
        self.master = master
        self.initUI()
        self.create_buttons()
        self.create_editor()

    def initUI(self):
        # mengatur hal-hal mengenai window aplikasi. Nilai master berisi master dari class ini yaitu Tk()
        # mengatur title aplikasi
        self.master.title("Pacil Editor")
        # mengatur ukuran inisialisasi aplikasi
        self.master.geometry("900x600")
        # meletakkan frame ini ke dalam window dengan metode pack
        self.pack(fill=BOTH, expand=True, side=TOP)
        # mengatur weight untuk row
        self.grid_rowconfigure(1, weight=1)
        # mengatur weight untuk column
        self.grid_columnconfigure(0, weight=1)

        # membuat frame baru untuk tombol-tombol. Kita juga dapat mengatur warna backgroundnya
        self.btnCont = Frame(self, bg="#252526")
        # meletakkan frame tersebut dengan metode grid. kita mengatur letaknya dengan menggunakan
        # row dan column.
        self.btnCont.grid(row=0, column=0, sticky="new")

        pass

    def create_buttons(self):
        # membuat widget button. Disini kita dapat mengatur tulisan pada button
        # dan juga perintah yang dilaksanakan ketika button ditekan.
        # kita meletakkan button-button ini dalam frame btnCont dengan metode pack
        self.openFile = Button(
            self.btnCont, text="Open File", command=self.load_file)
        self.openFile.pack(side=LEFT, padx=5, pady=5)

        self.saveFile = Button(
            self.btnCont, text="Save File", command=self.save_file)
        self.saveFile.pack(side=LEFT, padx=5, pady=5)

        self.closeApp = Button(
            self.btnCont, text="Quit Program", command=self.master.quit)
        self.closeApp.pack(side=LEFT, padx=5, pady=5)
        pass

    def create_editor(self):
        # kita membuat text box. Kita dapat mengatur warna dari background, foreground dan juga warna
        # dari cursor teks. Kita meletakkan text box ini langsung ke dalam frame dasar dengan metode grid
        self.textEdit = Text(
            self, bg="#333333", fg="#FFFFFF", insertbackground="#FFFFFF")
        self.textEdit.grid(
            row=1, column=0, sticky="nsew", padx=5, pady=5)
        self.textEdit.grid_columnconfigure(0, weight=1)
        self.textEdit.grid_rowconfigure(0, weight=1)
        pass

    def load_file_event(self, event):
        self.load_file()

    def load_file(self):
        # metode untuk membuka file
        file_name = askopenfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        text_file = open(file_name, 'r', encoding="utf-8")
        result = text_file.read()
        text_file.close()
        self.set_text(text=result)

    def save_file_event(self, event):
        self.save_file()

    def save_file(self):
        file_name = asksaveasfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return

        text = self.get_text()
        text_file = open(file_name, 'w', encoding="utf-8")
        text_file.write(text)
        text_file.close()

    def set_text(self, text=''):
        self.textEdit.delete('1.0', END)
        self.textEdit.insert('1.0', text)
        self.textEdit.mark_set(INSERT, '1.0')
        self.textEdit.focus()

    def get_text(self):
        return self.textEdit.get('1.0', END+'-1c')


if __name__ == "__main__":
    root = Tk()
    app = Application(master=root)
    app.mainloop()

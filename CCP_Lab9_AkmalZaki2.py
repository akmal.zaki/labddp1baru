#Nama  = Akmal Zaki
#Kelas = DDP-1
#Npm   = 2106752205

import datetime
from tempfile import gettempprefix


# membuat class tokoHomura
class TokoHomura:
    # beberapa properti tokoHomura didefinisikan
    # properti ini berisi informasi penting tentang toko Homura
    listKaryawan = []
    activeKaryawan = None
    programBerjalan = True
    jumlahDetergen = 0
    jumlahKue = 0
    pendapatan = 0
    tanggalDibersihkan = ""

    # membuat setter dan getter property agar sesaui dengan kaidah oop
    def getPendapatan(self):
        return self.pendapatan

    def setPendapatan(self, pendapatan):
        self.pendapatan = pendapatan

    def getJumlahDetergen(self):
        return self.jumlahDetergen

    def setJumlahDetergen(self, jumlahDetergen):
        self.jumlahDetergen = jumlahDetergen

    def getJumlahKue(self):
        return self.jumlahKue

    def setJumlahKue(self, jumlahKue):
        self.jumlahKue = jumlahKue

    def getTanggalDibersihkan(self):
        return self.tanggalDibersihkan

    def setTanggalDibersihkan(self, tanggalDibersihkan):
        self.tanggalDibersihkan = tanggalDibersihkan

    def __init__(self):
        pass

    # mencetak menu toko Homura ketika tidak ada akun yang login
    def cetakMenu(self):
        print("\nSelamat datang di Sistem Manajemen Homura\n")
        print("Apa yang ingin anda lakukan? (Tulis angka saja)")
        print("1. Register karyawan baru")
        print("2. Login")
        print("8. Status Report")
        print("9. Karyawan Report")
        print("11. Exit")
        menu = input("\nPilihan: ")
        if int(menu) == 1:
            print(
                "Format data: [username] [password] [nama] [umur] [role]")
            data = input("Input data karyawan baru: ")
            self.registrasi(data)
        elif int(menu) == 2:
            username = input("Username: ")
            password = input("Password: ")
            self.login(username, password)
        elif int(menu) == 8:
            self.statusReport()
        elif int(menu) == 9:
            self.karyawanReport()
        elif int(menu) == 11:
            print("TERIMA KASIH TELAH MENGGUNAKAN SISTEM MANAJEMEN HOMURA")
            self.exit()
        else:
            self.main()

    # mencetak menu kasir
    def cetakMenuKasir(self):
        print("\nApa yang ingin anda lakukan? (Tulis angka saja)")
        print("3. Terima pembayaran")
        print("10. Logout")
        menu = input("\nPilihan: ")
        if int(menu) == 3:
            uang = int(input("Jumlah pembayaran: "))
            self.activeKaryawan.terimaUang(uang)
            self.setPendapatan(self.getPendapatan() + uang)
            print(f"Berhasil menerima uang sebanyak {uang}")
            self.main()
        elif int(menu) == 10:
            self.activeKaryawan = None
            self.main()
        else:
            self.main()

    # mencetak menu janitor
    def cetakMenuJanitor(self):
        print("\nApa yang ingin anda lakukan? (Tulis angka saja)")
        print("4. Bersihkan toko")
        print("5. Beli deterjen")
        print("10. Logout")
        menu = input("\nPilihan: ")
        if int(menu) == 4:
            self.setJumlahDetergen(
                self.activeKaryawan.bersihkanToko(self.getJumlahDetergen()))
            self.setTanggalDibersihkan(datetime.datetime.now())
            self.main()
        elif int(menu) == 5:
            banyakDetergen = int(input("Jumlah pembelian deterjen: "))
            self.setJumlahDetergen(self.activeKaryawan.beliDetergen(
                self.getJumlahDetergen(), banyakDetergen))
            print(f"Berhasil membeli {banyakDetergen} deterjen")
            self.main()
        elif int(menu) == 10:
            self.activeKaryawan = None
            self.main()
        else:
            self.main()

    def cetakMenuChef(self):
        print("\nApa yang ingin anda lakukan? (Tulis angka saja)")
        print("6. Buat kue")
        print("7. Buang kue")
        print("10. Logout")
        menu = input("\nPilihan: ")
        if int(menu) == 6:
            kue = int(input("Buat berapa kue?: "))
            self.setJumlahKue(self.getJumlahKue() + kue)
            self.activeKaryawan.tambahKueDibuat(kue)
            print(f"Berhasil membuat {kue} kue")
            self.main()
        elif int(menu) == 7:
            kue = int(input("Buang berapa kue?: "))
            if (kue > self.getJumlahKue()):
                print(
                    "Tidak bisa membuang lebih banyak kue dibandingkan dengan stok kue yang ada sekarang")
            else:
                self.activeKaryawan.tambahKueDibuang(kue)
                self.setJumlahKue(self.getJumlahKue() - kue)
                print(f"Berhasil membuang {kue} kue")
            self.main()
        elif int(menu) == 10:
            self.activeKaryawan = None
            self.main()
        else:
            self.main()

    # proses registrasi dengan mengambil data-data dari string input
    def registrasi(self, data):
        username, password, nama, umur, role = data.split(" ")
        index = self.cariKaryawan(username)
        if index == -1:
            if (role == "Kasir"):
                kasir = Kasir(username, password, nama, umur)
                self.listKaryawan.append(kasir)
            elif (role == "Janitor"):
                janitor = Janitor(username, password, nama, umur)
                self.listKaryawan.append(janitor)
            elif (role == "Chef"):
                chef = Chef(username, password, nama, umur)
                self.listKaryawan.append(chef)
            print(f"Karyawan {nama} berhasil ditambahkan")
        else:
            print(f"Karyawan {nama} sudah pernah ditambahkan")
        self.main()

    # proses login
    def login(self, username, password):
        index = self.cariKaryawan(username)
        if(index != -1):
            if(self.listKaryawan[index].password == password):
                self.activeKaryawan = self.listKaryawan[index]
                self.activeKaryawan.setTerakhirLogin(datetime.datetime.now())
                print(f"Selamat datang {self.activeKaryawan.nama}")
            else:
                print("Username atau pasword salah\n")
        else:
            print("Username atau pasword salah\n")
        self.main()

    # mendapatkan attribut2 dari toko Homura
    def statusReport(self):
        print("===================================")
        print("STATUS TOKO HOMURA SAAT INI")
        print(f"Jumlah Karyawan: {len(self.listKaryawan)}")
        print(f"Jumlah Cash: {self.getPendapatan()}")
        print(f"Jumlah Kue: {self.getJumlahKue()}")
        print(f"Jumlah deterjen: {self.getJumlahDetergen()}")
        print(f"Terakhir kali dibersihkan: {self.getTanggalDibersihkan()}")
        print("===================================")
        self.main()

    # mencetak report karyawan toko Homura
    def karyawanReport(self):
        for i in range(len(self.listKaryawan)):
            print("===================================")
            print(self.listKaryawan[i])
            print("===================================")
        self.main()

    # done
    def exit(self):
        self.programBerjalan = False

    def main(self):
        if self.programBerjalan == True:
            # memeriksa apakah sudah terjadi login atau belum
            if(self.activeKaryawan == None):
                self.cetakMenu()
            # jika sudah login
            else:
                # memeriksa jika yang login adalah kasir
                if(self.activeKaryawan.__class__.__name__ == "Kasir"):
                    self.cetakMenuKasir()
                elif(self.activeKaryawan.__class__.__name__ == "Janitor"):
                    self.cetakMenuJanitor()
                elif(self.activeKaryawan.__class__.__name__ == "Chef"):
                    self.cetakMenuChef()

    def cariKaryawan(self, username):
        for i in range(len(self.listKaryawan)):
            if (self.listKaryawan[i].username == username):
                return i
        return -1


# class parent karyawan
class Karyawan:
    username = ""
    password = ""
    nama = ""
    umur = 0
    terakhirLogin = ""

    def setTerakhirLogin(self, terakhirLogin):
        self.terakhirLogin = terakhirLogin

    def getTerakhirLogin(self):
        return self.terakhirLogin

    def __init__(self, username, password, nama, umur):
        self.username = username
        self.password = password
        self.nama = nama
        self.umur = umur

    def __str__(self) -> str:
        return f"Username: {self.username}\nNama: {self.nama}\nUmur: {self.umur}\nLogin Terakhir: {self.terakhirLogin}\n"


# class kasir yang merupakan anak dari class karyawan
class Kasir(Karyawan):
    uangDiterima = 0

    def terimaUang(self, uang: int):
        self.uangDiterima += uang

    def __str__(self) -> str:
        return super().__str__() + f"Role: {self.__class__.__name__}\nJumlah uang diterima: {self.uangDiterima}"


# class janitor yang merupakan anak dari class karyawan
class Janitor(Karyawan):
    jumlahMembersihkanToko = 0
    jumlahDetergenDibeli = 0

    def bersihkanToko(self, banyakDetergen):
        if(banyakDetergen > 0):
            self.jumlahMembersihkanToko += 1
            return banyakDetergen - 1
        else:
            print("Diperlukan setidaknya 1 deterjen untuk melakukan pembersihan\n")
            return 0

    def beliDetergen(self, banyakDetergen, penambahan):
        self.jumlahDetergenDibeli += penambahan
        return banyakDetergen + penambahan

    def __str__(self) -> str:
        return super().__str__() + f"Role: {self.__class__.__name__}\nJumlah berapa kali membersihkan toko: {self.jumlahMembersihkanToko}\nJumlah deterjen yang telah dibeli: {self.jumlahDetergenDibeli}"


class Chef(Karyawan):
    jumlahKueDibuat = 0
    jumlahKueDibuang = 0

    def tambahKueDibuat(self, jumlah):
        self.jumlahKueDibuat += jumlah

    def tambahKueDibuang(self, jumlah):
        self.jumlahKueDibuang += jumlah

    def __str__(self) -> str:
        return super().__str__() + f"Role: {self.__class__.__name__}\nJumlah kue yang telah dibuat: {self.jumlahKueDibuat}\nJumlah kue yang telah dibuang: {self.jumlahKueDibuang}"


# menjalankan toko homura
toko = TokoHomura()
toko.main()
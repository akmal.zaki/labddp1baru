#Nama  = Akmal Zaki
#Kelas = DDP-1
#Npm   = 21067522051

from typing import List


def cetak_daftar_perintah():
    print("Berikut adalah daftar perintah yang dapat anda lakukan")
    print("1. Mengecek apakah x adalah elemen ke N dari suatu list L")
    print("2. Mengecek apakah x merupakan elemen dari suatu list L")
    print("3. Mengecek apakah 2 buah List memiliki urutan yang saling berkebalikan")
    print("4. Mengecek apakah sebuah List Palindrome, yaitu memiliki urutan yang sama ketika dibaca dari kanan maupun kiri")
    print("5. Mendapatkan semua elemen unik dari sebuah List")
    print("9. Exit()")
    return input("Masukkan nomor perintah saja:\n")


def input_list():
    return input("Masukkan semua elemen dari L yang dipisahkan oleh karakter spasi \nMisalnya ['2','a','?'], maka masukkan 2 a ?\n").split()


def cetak_perintah1():
    print("Anda memilih perintah 1, yaitu: ")
    print("1. Mengecek apakah x adalah elemen ke N dari suatu list L")
    print("Asumsikan x dan semua elemen di L memiliki tipe string")
    x = input("Masukkan x: ")
    L = input_list()
    n = int(input("Masukkan nilai N (1<=N<=Banyak elemen L):"))
    while n > len(L) or n <= 0:
        print("Nilai N>Banyak elemen L atau kurang dari 1")
        n = int(input("Masukkan kembali nilai N (1<=N<=Banyak elemen L):"))

    hasil = check_nth_element_of_list(x, L, n-1)
    if hasil:
        print(f"{x} adalah elemen ke {n} dari List {L}")
    else:
        print(f"{x} bukan elemen ke {n} dari List {L}")


def cetak_perintah2():
    print("Anda memilih perintah 2, yaitu: ")
    print("2. Mengecek apakah x merupakan elemen dari suatu list L")
    print("Asumsikan x dan semua elemen di L memiliki tipe string")
    x = input("Masukkan x: ")
    L = input_list()

    hasil = member(x, L)
    if hasil:
        print(f"{x} adalah elemen dari List {L}")
    else:
        print(f"{x} bukan elemen dari List {L}")


def cetak_perintah3():
    print("Anda memilih perintah 3, yaitu: ")
    print("3. Mengecek apakah 2 buah List memiliki urutan yang saling berkebalikan")
    print("Asumsikan  semua elemen di kedua list memiliki tipe string")

    L1 = input_list()
    L2 = input_list()

    # perhatikan bahwa L3 memiliki default nilai [], sehingga hanya perlu diisi 2 parameter di line ini
    hasil = reverse(L2, L1)
    if hasil:
        print(f"{L1} adalah reverse dari {L2} ")
    else:
        print(f"{L1} bukan reverse dari {L2} ")


def cetak_perintah4():
    print("Anda memilih perintah 4, yaitu: ")
    print("4. Mengecek apakah sebuah List Palindrome, yaitu memiliki urutan yang sama ketika dibaca dari kanan maupun kiri")
    print("Asumsikan  semua elemen pada list memiliki tipe string")

    L = input_list()
    hasil = palindrome(L)
    if hasil:
        print(f"{L} adalah List Palindrome ")
    else:
        print(f"{L} bukan List Palindrome ")


def cetak_perintah5():
    print("Anda memilih perintah 5, yaitu: ")
    print("5. Mendapatkan semua elemen unik dari sebuah List")
    print("Asumsikan  semua elemen pada list memiliki tipe string")
    LTemp = []
    L = input_list()
    hasil = list_to_set(L, LTemp)

    print(f"{hasil} berisi semua elemen dari list {L} tanpa ada duplikasi ")

# fungsi new_prepend(L,x) TIDAK BOLEH DIMODIFIKASI

def new_prepend(L, x):
    # '''
    # method helper untuk menambahkan suatu elemen x ke list L sehingga x adalah element pertama dari L dalam proses rekursif
    # '''
    res = [x]
    res.extend(L)
    return res

# fungsi new_append(L,x) TIDAK BOLEH DIMODIFIKASI

def new_append(L, x):
    # '''
    # method helper untuk menambahkan suatu elemen x ke list L sehingga x adalah element terakhir dari L dalam proses rekursif
    # '''
    L.append(x)
    return L

# Task 1, sudah dikerjakan
def check_nth_element_of_list(x, L, n):
    # mengecek apakah x merupakan elemen ke-n dari list L, kembalikan nilai kebenarannya
    if n == 0:
        return x == L[0]
    else:
        return check_nth_element_of_list(x, L[1:],  n-1)

# Task 2
def member(x, L):  # true kalo si x adalah anggota L
    # '''
    # Mengecek apakah x adalah elemen dari suatu list L
    # '''

    # mengecek apakah terdapat elemen pada array
    if (len(L) > 0):
      # jika masih belum index terakhir lakukan pengecekan
      if len(L) >= 1:
          if x == L[0]: # kembalikan true jika elemen ditemukan
              return True
          elif x != L[0]: # jika elemen belum ditemukan
              if len(L) == 1: # jika sudah index terakhir maka elemen tidak ditemukan
                 return False
              else: # jika belum index terakhir, cari kembali
                 return member(x, L[1:])
    else: # return false jika array kosong
      return False

# Task 3
# harus rekursif dan L1 tidak boleh dimodifikasi
def reverse(L1, L2, L3=[]):  # cek apakah si L1 hasil reversenya adalah L2 dengan asumsi len(L1)==Len(L2)
    # '''
    # Mengecek apakah L1 merupakan reverse dari elemen L2 atau dengan kata lain, urutan di L2 berkebalikan dengan urutan di L1
    # '''
     
   if len(L2) > 0 and  len(L1) > 0: # jika belum index terakhir, lakukan pengecekan
      if L1[0] == L2[len(L2)-1]: # jika elemen reverse benar, lanjutkan pemeriksaan
          return reverse(L1[1:], L2[:len(L2)-1])
      else: # jika elemen reverse berbeda, langsung kembalikan false
          return False                    

   # jika elemen reverse benar semua hingga indeks terakhir,
   # kembalikan true
   return True


# Task 4
def palindrome(L):  # return true kalo urutan di L palindrome
    # '''
    # Mengecek apakah suatu list L memiliki urutan elemen sehingga merupakan list palindrome
    # '''
    
    # cek jika sudah berada pada index tengah dari array
    if(len(L)/2 >= 1):
        # lakukan pemeriksaan pada indeks pertama array reverse dan indeks pertama array 
        if L[::-1][0] == L[0]:
            # jika betul, lanjutkan pemeriksaan pada elemen berikut nya
            return palindrome(L[1:len(L)-1])
        else: # jika ada elemen yang berbeda, maka bukan palindrome
            return False
    return True
# Task 5
# list 1 adalah list yang ingin diubah jadi set
# list 2 adalah list yang berisi semua element di List 1 tanpa duplikasi
# harus rekursi, dan yang direturn adalah nilai akhir L2


def list_to_set(List1, List2=[]):
    # '''
    # Mengubah list menjadi set
    # '''
    
    # memeriksa apakah List 1 adalah member dari List2
    if not member(List1[-1], List2):
        # jika bukan, maka tambahkan ke List2
        List2 = new_prepend(List2, List1[-1])

    if len(List1) > 1:
        # selama bukan elemen terakhir, tambahkan lagi
        return list_to_set(List1[:len(List1)-1], List2)
    else:
        # jika sudah elemen terakhir, kembalikan List2
        return List2


# Tidak perlu diubah
def main():
    print("Selamat datang di dunia Prolog")
    cekExit = False
    while not cekExit:
        nomor_perintah = cetak_daftar_perintah()
        if nomor_perintah == "9":
            print("Anda telah keluar dari dunia Prolog")
            cekExit = True
            return
        elif nomor_perintah == "1":
            cetak_perintah1()
        elif nomor_perintah == "2":
            cetak_perintah2()
        elif nomor_perintah == "3":
            cetak_perintah3()
        elif nomor_perintah == "4":
            cetak_perintah4()
        elif nomor_perintah == "5":
            cetak_perintah5()
        else:
            print(f"nomor masukan {nomor_perintah} tidak terdapat pada daftar")
        print("\n", "\n")
    return


main()

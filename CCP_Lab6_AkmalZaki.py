#Nama  = Akmal Zaki
#Kelas = DDP-1
#Npm   = 2106752205

def main_program():
    print("===============================")
    print("WELCOME TO KARTI-NIAN CHALLENGE")
    print("===============================")
    print()

    daftarLomba = []

    # meminta user untuk memasukkan banyak lomba
    banyakLomba = int(input("Masukkan jumlah acara yang akan dilombakan: "))

    print()

    for i in range(banyakLomba):
        lomba = []

        print("LOMBA " + str(i+1))
        # meminta user memasukkan nama lomba dan hadiah juara
        namaLomba = input("Nama lomba: ")
        hadiah1 = int(input("Hadiah juara 1: "))
        hadiah2 = int(input("Hadiah juara 2: "))
        hadiah3 = int(input("Hadiah juara 3: "))

        # menambahkan data-data tersebut ke dalam array lomba
        lomba.append(namaLomba)
        lomba.append(hadiah1)
        lomba.append(hadiah2)
        lomba.append(hadiah3)

        # menambahkan array lomba ke dalam daftar lomba
        daftarLomba.append(lomba)

        print()

    print("================================")
    print("  DAFTAR MATA LOMBA DAN HADIAH")
    print("================================")
    print()

    # melakukan sorting daftar lomba berdasarkan nama lomba
    daftarLomba = sortLombaDariNama(daftarLomba=daftarLomba)

    # mencetakk nama lomba dan hadiahnya
    for i in range(len(daftarLomba)):
        print("Lomba " + daftarLomba[i][0])
        print("[Juara 1] " + str(daftarLomba[i][1]))
        print("[Juara 2] " + str(daftarLomba[i][2]))
        print("[Juara 3] " + str(daftarLomba[i][3]))
        print()

    print("================================")
    print("    DATA PESERTA CHALLENGE")
    print("================================")
    print()

    daftarPeserta = []

    # meminta user memasukkan banyak peserta
    banyakPeserta = int(input("Masukkan jumlah peserta yang berpartisipasi: "))
    print()

    for i in range(banyakPeserta):
        peserta = []
        # meminta user memasukkan nama peserta
        namaPeserta = input("Nama Peserta " + str(i + 1) + ": ")

        # mencari indeks peserta berdasarkan nama peserta
        pesertaIdx = findPesertaDariNama(
            daftarPeserta=daftarPeserta, namaPeserta=namaPeserta)

        # jika tidak ditemukan, maka menambahkan peserta ke dalam daftar peserta
        if(pesertaIdx == -1):
            peserta.append(namaPeserta)
            peserta.append(0)
            daftarPeserta.append(peserta)

        # jika ditemukan, maka nama peserta berubah sesuai dengan yang terakhir di input
        else:
            if(len(daftarPeserta) > 0):
                daftarPeserta[pesertaIdx][0] = namaPeserta

    print("================================")
    print("        CHALLENGE DIMULAI       ")
    print("================================")
    print()

    for i in range(len(daftarLomba)):
        print("Lomba " + daftarLomba[i][0])

        # meminta user memasukkan nama pemenang. Jika tidak ditemukan, maka program akan meminta untuk memasukkan kembali
        juara1 = input("Juara 1: ")
        juara1Idx = findPesertaDariNama(
            daftarPeserta=daftarPeserta, namaPeserta=juara1)
        while juara1Idx == -1:
            print("Maaf nama peserta tidak ditemukan. Masukkan kembali")
            juara1 = input("Juara 1: ")
            juara1Idx = findPesertaDariNama(
                daftarPeserta=daftarPeserta, namaPeserta=juara1)

        juara2 = input("Juara 2: ")
        juara2Idx = findPesertaDariNama(
            daftarPeserta=daftarPeserta, namaPeserta=juara2)
        while juara2Idx == -1:
            print("Maaf nama peserta tidak ditemukan. Masukkan kembali")
            juara2 = input("Juara 2: ")
            juara2Idx = findPesertaDariNama(
                daftarPeserta=daftarPeserta, namaPeserta=juara2)

        juara3 = input("Juara 3: ")
        juara3Idx = findPesertaDariNama(
            daftarPeserta=daftarPeserta, namaPeserta=juara3)
        while juara3Idx == -1:
            print("Maaf nama peserta tidak ditemukan. Masukkan kembali")
            juara3 = input("Juara 3: ")
            juara3Idx = findPesertaDariNama(
                daftarPeserta=daftarPeserta, namaPeserta=juara3)

        # memasukkan juara sesuai dengan indeks peserta serta menghitung total hadiah
        daftarPeserta[juara1Idx].append(1)
        daftarPeserta[juara1Idx][1] += daftarLomba[i][1]

        daftarPeserta[juara2Idx].append(2)
        daftarPeserta[juara2Idx][1] += daftarLomba[i][2]

        daftarPeserta[juara3Idx].append(3)
        daftarPeserta[juara3Idx][1] += daftarLomba[i][3]

        print()

    print("================================")
    print("           FINAL RESULT         ")
    print("================================")
    print()

    # mengurutkan peserta berdasarkan nama peserta
    daftarPeserta = sortPesertaDariNama(daftarPeserta=daftarPeserta)

    for i in range(len(daftarPeserta)):
        print(daftarPeserta[i][0])
        print("Total hadiah: " + str(daftarPeserta[i][1]))
        print("Juara yang pernah diraih: ", end='')
        for j in range(2, len(daftarPeserta[i])):
            print(daftarPeserta[i][j], end=' ')

        print()
        print()


def sortLombaDariNama(daftarLomba):
    mark = 0
    for i in range(len(daftarLomba)):
        mark = i
        for j in range(i, len(daftarLomba)):
            if(daftarLomba[mark][0] > daftarLomba[j][0]):
                mark = j

        tmp = daftarLomba[i]
        daftarLomba[i] = daftarLomba[mark]
        daftarLomba[mark] = tmp

    return daftarLomba


def sortPesertaDariNama(daftarPeserta):
    mark = 0
    for i in range(len(daftarPeserta)):
        mark = i
        for j in range(i, len(daftarPeserta)):
            if(daftarPeserta[mark][0] > daftarPeserta[j][0]):
                mark = j

        tmp = daftarPeserta[i]
        daftarPeserta[i] = daftarPeserta[mark]
        daftarPeserta[mark] = tmp

    return daftarPeserta

# melakukan search dengan algoritma linear search
def findPesertaDariNama(daftarPeserta, namaPeserta):
    indeks = -1
    for i in range(len(daftarPeserta)):
        if(daftarPeserta[i][0].lower() == namaPeserta.lower()):
            indeks = i

    return indeks

main_program()

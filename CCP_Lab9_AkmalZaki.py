#Nama  = Akmal Zaki
#Kelas = DDP-1
#Npm   = 2106752205

import datetime

# membuat class tokoHomura


class TokoHomura:
    # beberapa properti tokoHomura didefinisikan
    listKaryawan = []
    activeKaryawan = None
    programBerjalan = True
    jumlahDetergen = 0
    pendapatan = 0
    tanggalDibersihkan = ""

    # membuat setter dan getter property
    def getPendapatan(self):
        return self.pendapatan

    def setPendapatan(self, pendapatan):
        self.pendapatan = pendapatan

    def __init__(self):
        pass

    # mencetak menu
    def cetakMenu(self):
        print("\nSelamat datang di Sistem Manajemen Homura\n")
        print("Apa yang ingin anda lakukan? (Tulis angka saja)")
        print("1. Register karyawan baru")
        print("2. Login")
        print("8. Status Report")
        print("9. Karyawan Report")
        print("11. Exit")
        menu = input("\nPilihan: ")
        if int(menu) == 1:
            print(
                "Format data: [username] [password] [nama] [umur] [role]")
            data = input("Input data karyawan baru: ")
            self.registrasi(data)
        elif int(menu) == 2:
            username = input("Username: ")
            password = input("Password: ")
            self.login(username, password)
        elif int(menu) == 8:
            self.statusReport()
        elif int(menu) == 9:
            self.karyawanReport()
        elif int(menu) == 11:
            self.exit()
        else:
            self.main()

    # mencetak menu kasir
    def cetakMenuKasir(self):
        print("Apa yang ingin anda lakukan? (Tulis angka saja)")
        print("3. Terima pembayaran")
        print("10. Logout")
        menu = input("\nPilihan: ")
        if int(menu) == 3:
            uang = input("Jumlah pembayaran: ")
            self.activeKaryawan.terimaUang(uang)
            self.setPendapatan(self.getPendapatan + uang)
            print(f"Berhasil menerima uang sebanyak {uang}")
            self.main()
        elif int(menu) == 10:
            self.activeKaryawan = None
            self.main()

    # proses registrasi
    def registrasi(self, data):
        username, password, nama, umur, role = data.split(" ")
        index = self.cariKaryawan(username)
        if index == -1:
            if (role == "Kasir"):
                kasir = Kasir(username, password, nama, umur)
                self.listKaryawan.append(kasir)
            elif (role == "Janitor"):
                janitor = Janitor(username, password, nama, umur)
                self.listKaryawan.append(janitor)
            elif (role == "Chef"):
                chef = Chef(username, password, nama, umur)
                self.listKaryawan.append(chef)
            print(f"Karyawan {nama} berhasil ditambahkan")
        else:
            print(f"Karyawan {nama} sudah pernah ditambahkan")
        self.main()

    # proses login
    def login(self, username, password):
        index = self.cariKaryawan(username)
        if(index != -1):
            if(self.listKaryawan[index].password == password):
                self.activeKaryawan = self.listKaryawan[index]
                print(f"Selamat datang {self.activeKaryawan.nama}")
            else:
                print("Username atau pasword salah\n")
        else:
            print("Username atau pasword salah\n")
        self.main()

    # mencetak status report
    def statusReport(self):
        pass

    # mencetak report karyawan
    def karyawanReport(self):
        print(self.listKaryawan[0].username)

    # done
    def exit(self):
        self.programBerjalan = False

    def main(self):
        if self.programBerjalan == True:
            # memeriksa apakah sudah terjadi login atau belum
            if(self.activeKaryawan == None):
                self.cetakMenu()
            # jika sudah login
            else:
                # memeriksa jika yang login adalah kasir
                if(self.activeKaryawan.__class__.__name__ == "Kasir"):
                    self.cetakMenuKasir()

    def cariKaryawan(self, username):
        for i in range(len(self.listKaryawan)):
            if (self.listKaryawan[i].username == username):
                return i
        return -1


# class parent karyawan
class Karyawan:
    username = ""
    password = ""
    nama = ""
    umur = 0

    def __init__(self, username, password, nama, umur):
        self.username = username
        self.password = password
        self.nama = nama
        self.umur = umur


# class kasir yang merupakan anak dari class karyawan
class Kasir(Karyawan):
    uangDiterima = 0

    def terimaUang(self, uang: int):
        uangDiterima += uang


# class janitor yang merupakan anak dari class karyawan
class Janitor(Karyawan):

    def bersihkanToko(self, banyakDetergen):
        if(banyakDetergen > 0):
            return banyakDetergen-1
        else:
            print("Diperlukan setidaknya 1 deterjen untuk melakukan pembersihan\n")


# menjalankan toko homura
toko = TokoHomura()
toko.main()